class Elem {
    constructor (matrix, coords, value) {
        this.matrix = matrix;
        this.coords = coords;
        this.value = '';
    }

    show () {
        for (let coord of this.coords) {
            this.matrix.setCell(coord[0], coord[1], this.value);
        }
    }
}