window.addEventListener('load', () => {

    let score = 0;

    let scoreField = document.querySelector('.score-field');
    let div = document.querySelector('.field');

    let matrix = new Matrix(div, 20, 20);
    let snake = new Snake(matrix, [[5, 4], [4, 4], [3, 4]], 'right');

    matrix.create();

    (new Wall(matrix, [[10, 5], [10, 6], [10, 7], [10, 8], [10, 9]])).show();
    (new Wall(matrix, [[2, 12], [3, 12], [4, 12], [5, 12], [6, 12]])).show();
    (new Wall(matrix, [[12, 14], [12, 15], [12, 16], [12, 17], [12, 18]])).show();
    (new Fruit(matrix, [[4, 2]])).show();
    snake.show();

    window.addEventListener('keydown', e => {
       switch (e.code) {
           case 'ArrowLeft':
               if (snake.course !== 'right')
               snake.newCourse = 'left';
               break;
           case 'ArrowUp':
               if (snake.course !== 'down')
               snake.newCourse = 'up';
               break;
           case 'ArrowRight':
               if (snake.course !== 'left')
               snake.newCourse = 'right';
               break;
           case 'ArrowDown':
               if (snake.course !== 'up')
               snake.newCourse = 'down';
               break;
       }
    });

    let interval = setInterval(() => {
        snake.move();

        if (!snake.alive) {
            clearInterval(interval);
            alert('Game over');
        }

        if (snake.eaten === true) {
            score++;
            scoreField.innerHTML = score;
            let x, y;

            do {
                x = Helper.random(1, matrix.x);
                y = Helper.random(1, matrix.y);
            } while (matrix.getCell(x, y) !== '');

            (new Fruit(matrix, [[x, y]])).show();
        }
    }, 100);
});