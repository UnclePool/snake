class Matrix {
    constructor (elem, x = 20, y = 20) {
        this.elem = elem;
        this.x = x;
        this.y = y;
        this.cell = []
    }

    create () {
        let fieldWidth = this.x * this.y;
        for (let i = 0; i < fieldWidth; i++) {
            let div = document.createElement('div');
            this.elem.append(div);
            this.cell[i] = '';
        }

        this.elem.style.width = `${this.x * 20}px`
    }

    getCell (x, y) {
        let num = this._calcNum(x, y);
        return this.cell[num];
    }

    setCell (x, y, val) {
        let num = this._calcNum(x, y);
        this.cell[num] = val;
        this.elem.children[num].className = val;
    }

    _calcNum (x, y) {
        return (y - 1) * this.y + x - 1
    }
}